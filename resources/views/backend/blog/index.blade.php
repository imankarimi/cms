@extends('layouts.app')

@section('content')
    <div class="container spark-screen">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if(!isset($show_post))
                    <div class="create-post"><a href="{{ URL::route('blog.create') }}" class="btn btn-success"><i class="fa fa-plus-square"></i> Add Post</a></div>
                    <br>
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">@if(isset($show_post)) {!! $show_post->title !!} @else List Of Posts @endif</div>

                    @if(isset($show_post))
                        <div class="panel-body">
                            {!! $show_post->body !!}
                        </div>
                        <hr>
                        <p style="text-align: center;">Author : {!! $show_post->user_name !!} | <a href="{{ URL::route('blog.index') }}">Back</a></p>
                    @else
                        @if(Session::has('message'))
                            <div class="panel-body">
                                <div id="message" class="input-group">
                                    <div class="btn-group" aria-describedby="basic-addon1">{!! Session::get('message') !!}</div>
                                    {{--<span class="btn-group fa fa-times close input-group-addon" id="basic-addon1"></span>--}}
                                </div>
                            </div>
                        @endif

                        <div class="panel-body">
                            @if($posts->count())
                                <table class="table table-striped table-bordered table-hover publish">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date & Time</th>
                                        <th>Title</th>
                                        <th>Show Post</th>
                                        <th>Edit Post</th>
                                        <th>Delete Post</th>
                                        <th>Publish</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php $i=1; ?>
                                    @foreach($posts as $post)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $post->date_time }}</td>
                                            <td>{{ substr($post->title, 0, 80) }}</td>
                                            <td>
                                                <center>
                                                    <a href="{{ URL::route('blog.show', $post->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i> Show</a>
                                                </center>
                                            </td>
                                            <td>
                                                <center>
                                                    <a href="{{ URL::route('blog.edit', $post->id) }}" class="btn btn-info"><i class="fa fa-edit"></i> Edit</a>
                                                </center>
                                            </td>
                                            <td class="del">
                                                <center>
                                                    {!! Form::open(['method' => 'DELETE', 'route' => ['blog.destroy', $post->id]]) !!}
                                                    <button class='btn btn-danger' type="submit" value="delete"><span class="fa fa-trash-o"></span> Delete</button>
                                                    {!! Form::close() !!}
                                                </center>
                                            </td>
                                            <td>
                                                <a class="status btn {!! (isset($post->status) && $post->status) ? 'btn-success' : 'btn-warning' !!}" data-token="{!! csrf_token() !!}" data-value="{!! $post->status !!}" data-url="#"><i class="fa {!! (isset($post->status) && $post->status) ? 'fa-eye' : 'fa-eye-slash' !!}"></i> {!! (isset($post->status) && $post->status) ? 'Published' : 'Unpublish' !!} </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                <div class="paging">
                                    <center>
                                        {!! $posts->render() !!}
                                    </center>
                                </div>
                            @else
                                Nothing found!
                            @endif
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection