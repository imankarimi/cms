@extends('layouts.app')

@section('content')
    <div class="container spark-screen">
        <div class="row" style="margin-bottom: 100px;">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">@if(isset($post)) Edit post @else Create new post @endif</div>

                    @if(Session::has('message'))
                        <div class="panel-body">
                            <div id="message" class="input-group">
                                <div class="btn-group" aria-describedby="basic-addon1">{!! Session::get('message') !!}</div>
                                {{--<span class="btn-group fa fa-times close input-group-addon" id="basic-addon1"></span>--}}
                            </div>
                        </div>
                    @endif

                    <div class="panel-body">
                        @if(isset($post))
                            {!! Form::model($post, ['method' => 'PUT', 'route' => ['blog.update', $post->id], 'files' => true]) !!}
                        @else
                            {!! Form::open(['method' => 'POST', 'route' => 'blog.store', 'files' => true]) !!}
                        @endif
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Title :</span>
                                    {!! Form::text('title', null, ['class'=>'form-control title', 'aria-describedby'=>'basic-addon1']) !!}
                                </div>
                                <br><br>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon2">Body :</span>
                                    {!! Form::textarea('body', null, ['class'=>'form-control', 'aria-describedby'=>'basic-addon2']) !!}
                                </div>
                                <br><br>

                                <div class="input-group">
                                    <div class="btn-group group-post-btn">
                                        @if(isset($post))
                                            {!! Form::submit('Update Post', ['class' => 'btn btn-info submit-send-post']) !!}
                                        @else
                                            {!! Form::submit('Save Post', ['class' => 'btn btn-info submit-send-post']) !!}
                                        @endif
                                    </div>
                                    <div class="btn-group group-post-btn"><a href="{{ URL::route('blog.index') }}" class="btn btn-danger callback-post">Cancel</a></div>
                                </div>

                            {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('') }}/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="{{ url('') }}/tinymce/tinymce_editor.js"></script>
    <script type="text/javascript">
        editor_config.selector      = "textarea";
        editor_config.path_absolute = "http://localhost:8000/";
        tinymce.init(editor_config);
    </script>
@endsection
