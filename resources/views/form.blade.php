@extends('layouts.app')

@section('content')
    <div class="container spark-screen">
        <div class="row" style="margin-bottom: 100px;">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    @if(Session::has('message'))
                        <div class="panel-body">
                            <div id="message" class="input-group">
                                <div class="btn-group" aria-describedby="basic-addon1">{!! Session::get('message') !!}</div>
                                {{--<span class="btn-group fa fa-times close input-group-addon" id="basic-addon1"></span>--}}
                            </div>
                        </div>
                    @endif
                   <div class="panel-body">
                       {!! Form::open(['method' => 'POST', 'route' => 'upload-image', 'files' => true]) !!}
                      <div class="input-group">
                          <span class="input-group-addon" id="basic-addon1">Title :</span>
                          <input type="text" name="title" placeholder="Title" />
                          @if($errors->has('title'))
                              {!!$errors->first('title')!!}
                          @endif
                      </div>
                      <br />
                      <div class="input-group">
                          <span class="input-group-addon" id="basic-addon1">name :</span>
                          <input type="text" name="sub_title" placeholder="Sub Title" />
                          @if($errors->has('sub_title'))
                              {!!$errors->first('sub_title')!!}
                          @endif
                      </div>
                       <br />

                      <input type="file" name="image">
                       @if($errors->has('image'))
                        {!!$errors->first('image')!!}
                      @endif
                     <br />
                    <input type="submit" value="Sumit" />
                  {!! form::close() !!}

                </div>
            </div>
        </div>
      </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('') }}/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="{{ url('') }}/tinymce/tinymce_editor.js"></script>
    <script type="text/javascript">
        editor_config.selector      = "textarea";
        editor_config.path_absolute = "http://localhost:8000/";
        tinymce.init(editor_config);
    </script>
@endsection
