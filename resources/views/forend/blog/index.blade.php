@extends('layouts.app')

@section('content')
    <div class="container spark-screen">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                {{--<div class="panel panel-default">--}}
                    @if(isset($show_post))
                        <div class="panel-body show-post">
                            <h2>{!! $show_post->title !!}</h2>
                            <p>{!! $show_post->body !!}</p>
                            <hr>
                            <p class="user-detail" style="text-align: center;"><span>Author : {!! $show_post->user_name !!}</span> | <a href="{{ URL::route('home') }}"><i class="fa fa-arrow-left"></i> Back</a></p>
                        </div>
                    @else

                        <div class="panel-body">
                            @if($posts->count())
                                <div class="pagination">
                                    @foreach($posts as $post)
                                        <div class="pagination">
                                            <div class="blog-post">
                                                <p><a href="{!! URL::route('show-post', $post->alias) !!}">{!! $post->title !!}</a></p>
                                                <p>{!! substr($post->body, 0, 400) !!} ...<a href="{!! URL::route('show-post', $post->alias) !!}"> read more</a></p>
                                                <div class="detail">
                                                    <ul>
                                                        <li><i class="fa fa-user"> {!! $post->user_name !!}</i></li>
                                                        <li><i class="fa fa-clock-o"> {!! $post->date_time !!}</i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <br><hr>
                                        </div>
                                    @endforeach

                                    <div class="panel pagination">
                                        {!! $posts->render() !!}
                                    </div>
                                </div>

                            @else
                                Nothing found!
                            @endif
                        </div>
                    @endif
                {{--</div>--}}
            </div>
        </div>
    </div>
@endsection