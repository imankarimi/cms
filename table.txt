<table class="table table-striped table-bordered table-hover publish">
                                    <thead>
                                    <tr>

                                        <th>Date & Time</th>
                                        <th>Title</th>
                                        <th>Author</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php $i=1; ?>
                                    @foreach($posts as $post)
                                        <tr>
                                            <td>{{ $post->date_time }}</td>
                                            <td><a href="{!!URL::route('post.show', $post->id)  !!} "class="">{{ substr($post->title, 0, 80) }}</a></td>
                                            <td>{!! $post->user_name !!}</td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>