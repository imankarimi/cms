<?php

namespace App\Http\Controllers;
use DB;
use App\Image;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;

class UploadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function getForm()
    {
        return view('form');
    }
	
	public function store(Request $data){
		$validation=Validator::make($data->all(),array(
		     'title'=>'required',
		     'sub_title'=>'required',
		     'image'=>'required|mimes:jpg,jpeg|max:100',
		));
		if($validation->fails()){
			return Redirect::to('/')->withErrors($validation);
		}else{
			$logo =$data->file('image');
			$upload='Upload/logo';
			$filename=$logo->getClientOriginalName();
			$success=$logo->move($upload,$filename);
			if($success){
					$table=new Image;
			        $table->title=$data->Input('title');
			        $table->sub_title=$data->Input('sub_title');
			        $table->image=$filename;
			        $table->save();
			        return Redirect::to('/pload')->with('Success','Data Sumitted');
			}
		
		}
		
	}
}

