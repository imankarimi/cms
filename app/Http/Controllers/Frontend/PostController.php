<?php namespace App\Http\Controllers\Frontend;

use App\Blog;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class PostController extends Controller{

    protected $user;
    protected $blog;

    public function __construct(Blog $blog, User $user){
        $this->user = $user;
        $this->blog = $blog;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Resource
     */
    public function index(){

        $posts = $this->blog->orderBy('id', 'DESC')->paginate(5);

        foreach($posts as $post){

            $post->date_time = date('Y-m-d H:i:s', $post->timestamp);
            $post->user_name = $this->user->find($post->user_id)->name;
        }

        return view('forend.blog.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Resource
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Resource
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $alias
     * @return Resource
     */
    public function show($alias){

        $show_post = $this->blog->where('alias', $alias)->first();
        $show_post->user_name = $this->user->find($show_post->user_id)->name;

        return view('forend.blog.index', compact('show_post'));


    }

    /**
     * Update the specified resource.
     *
     * @param int $id
     * @return Resource
     */
    public function edit($id)
    {
        //
    }

    /**
     * Display the specified resource in storage.
     *
     * @param int $id
     * @return Resource
     */
    public function update($id)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Resource
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Create alias for post
     *
     * @param int $id , $title
     * @return Resource
     */
    private function createAlias($id, $title){
        $strtolower_title = strtolower($title);
        $alias            = str_replace('–', ' ', $strtolower_title);
        $alias            = str_replace(' ', '-', $alias);
        $alias            = str_replace('--', '-', $alias);
        $alias            = str_replace('--', '-', $alias);
        $alias            = $id.'-'.$alias;

        return $alias;
    }
}