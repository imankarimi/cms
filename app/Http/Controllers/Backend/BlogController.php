<?php namespace App\Http\Controllers\Backend;

use App\Blog;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class BlogController extends Controller {

    protected $user;
    protected $blog;

    public function __construct(Blog $blog, User $user){
        $this->middleware('auth');
        $this->user = $user;
        $this->blog = $blog;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Resource
     */
    public function index(){
        $user_id = auth()->user()->id;
        $posts = $this->blog->where('user_id', $user_id)->orderBy('id', 'DESC')->paginate(5);

        foreach($posts as $post){

            $post->date_time = date('Y-m-d H:i:s', $post->timestamp);
        }


        return view('Backend.blog.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Resource
     */
    public function create(){
        return view('Backend.blog.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Resource
     */
    public function store(){
        $input               = Input::all();
        $valid               = Validator::make($input, $this->blog->rules);

        if($valid->fails()){
            return redirect('blog/create')
                ->withErrors($valid)
                ->withInput()
                ->with('message', 'There were validation errors!');
        }

        $input['user_id']    = auth()->user()->id;
        $input['timestamp']  = strtotime(date('Y-m-d H:i:s'));
        $input['status']     = 1;
        $blog                = $this->blog->fill($input);

        if($blog->save()){

            $input['alias']  = $this->createAlias($blog->id, $input['title']);
            $blog          = $this->blog->fill($input);
            $blog->save();

            return redirect()->route('blog.index')->with('message', 'Create Post Successfully.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Resource
     */
    public function show($id){
        $show_post = $this->blog->find($id);
        $show_post->user_name = $this->user->find($show_post->user_id)->name;

        return view('Backend.blog.index', compact('show_post'));
    }

    /**
     * Update the specified resource.
     *
     * @param int $id
     * @return Resource
     */
    public function edit($id){
        $post = $this->blog->find($id);

        return view('Backend.blog.form', compact('post'));
    }

    /**
     * Display the specified resource in storage.
     *
     * @param int $id
     * @return Resource
     */
    public function update($id){
        $input = Input::all();
        $valid = Validator::make($input, $this->blog->rules);

        if($valid->fails()){
            return redirect('blog/edit')
                ->withErrors($valid)
                ->withInput()
                ->with('message', 'There were validation errors!');
        }

        $input['alias'] = self::createAlias($input['title'], $id);

        $this->blog     = $this->blog->find($id);
        $post           = $this->blog->fill($input);

        if($post->save()){
            return redirect()->route('blog.index')->with('message', 'update successfully.');
        }
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Resource
     */
    public function destroy($id){
        $this->blog->find($id)->delete();

        return redirect()->route('blog.index');
    }

    /**
     * Create alias for post
     *
     * @param int $id, $title
     * @return Resource
     */
    private function createAlias($id, $title){
        $strtolower_title = strtolower($title);
        $alias            = str_replace('–', ' ', $strtolower_title);
        $alias            = str_replace(' ', '-', $alias);
        $alias            = str_replace('--', '-', $alias);
        $alias            = str_replace('--', '-', $alias);
        $alias            = $id.'-'.$alias;

        return $alias;
    }

}