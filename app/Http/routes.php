<?php

Route::get('/', ['as' => 'home', 'uses' => 'Frontend\PostController@index']);
Route::get('/post/{alias}', ['as' => 'show-post', 'uses' => 'Frontend\PostController@show']);

//Route::post('/post','UploadController@store');

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
    Route::get('pload','UploadController@getForm');
   //Route::post('Upload','UploadController@store');
    Route::post('/pload', ['as' => 'upload-image', 'uses' => 'UploadController@store']);
    Route::resource('blog', 'Backend\BlogController');
    Route::controller('filemanager', 'FilemanagerLaravelController');
});