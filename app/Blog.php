<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model {

    protected $table    = 'blog';
    protected $fillable = ['user_id', 'alias', 'title', 'body', 'timestamp', 'status'];
    public $rules       = [
        'title' => 'required',
        'body'  => 'required',
    ];

}